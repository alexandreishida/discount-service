require "discounts"
require "opentracing"

class Discounter
  DISCOUNTS = [
    Discounts::BlackFridayDiscount.new,
    Discounts::BirthdayDiscount.new
  ]

  MAX = 10.0

  def self.calculate(user)
    OpenTracing.start_active_span("Discounter.calculate") do |scope|
      total = DISCOUNTS.reduce(0.0) do |total, discount|
        total += discount.get(user)
        return MAX if total >= MAX
        total
      end
      return total
    end
  end
end

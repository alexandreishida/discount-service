require "mongo_holder"
require "opentracing"

class User
  attr_accessor :id, :date_of_birth, :first_name, :last_name

  def initialize(id: nil, date_of_birth: nil, first_name: nil, last_name: nil)
    @id = id
    @date_of_birth = date_of_birth
    @first_name = first_name
    @last_name = last_name
  end

  def self.find(id)
    user = nil

    OpenTracing.start_active_span("User.find") do |scope|
      mongo = MongoHolder::Instance
      doc = mongo[:users].find({_id: id}).first
      if doc
        user = User.new(id: doc[:_id], 
                        date_of_birth: doc[:date_of_birth].to_date,
                        first_name: doc[:first_name],
                        last_name: doc[:last_name])
      end
    end

    user
  end
end

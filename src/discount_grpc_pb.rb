# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: discount_grpc.proto

require 'google/protobuf'

Google::Protobuf::DescriptorPool.generated_pool.build do
  add_file("discount_grpc.proto", :syntax => :proto3) do
    add_message "discount_grpc.DiscountRequest" do
      optional :user_id, :string, 1
      optional :product_id, :string, 2
    end
    add_message "discount_grpc.DiscountResponse" do
      optional :discount, :double, 1
    end
  end
end

module DiscountGrpc
  DiscountRequest = Google::Protobuf::DescriptorPool.generated_pool.lookup("discount_grpc.DiscountRequest").msgclass
  DiscountResponse = Google::Protobuf::DescriptorPool.generated_pool.lookup("discount_grpc.DiscountResponse").msgclass
end

def load_path
    source_path = File.expand_path("../", __FILE__)
    $LOAD_PATH.unshift(source_path) unless $LOAD_PATH.include?(source_path)
end

load_path

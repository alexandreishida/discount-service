module Discounts
  class Discount
    attr_reader :value

    def initialize(value)
      @value = value
    end

    def applicable?(user)
      raise Error.new("method not implemented")
    end

    def get(user)
      return value if applicable?(user)
      0.0
    end
  end

  class BlackFridayDiscount < Discount
    def initialize()
      super(10.0)
    end

    def applicable?(user)
      today = Date.today
      today.day == 25 && today.month == 11
    end
  end

  class BirthdayDiscount < Discount
    def initialize()
      super(5.0)
    end

    def applicable?(user)
      today = Date.today
      user.date_of_birth.day == today.day &&
          user.date_of_birth.month == today.month
    end
  end
end

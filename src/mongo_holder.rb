require "config"
require "mongo"

Mongo::Logger.logger.level = Logger::INFO

module MongoHolder
  Instance = Mongo::Client.new("mongodb://#{Config::MONGO_HOST}:#{Config::MONGO_PORT}/#{Config::MONGO_DBNAME}")
end

require "opentracing"

require "discounter"
require "discount_grpc_services_pb"
require "user"

module GrpcServices
  class DiscountService < DiscountGrpc::DiscountService::Service
    def get_discount(discount_request, _unused_call)
      OpenTracing.start_active_span("GrpcServices::DiscountService.get_discount") do |scope|
        discount = 0.0

        if discount_request.user_id
          user = User.find(discount_request.user_id)
          discount = Discounter.calculate(user) if user
        end

        return DiscountGrpc::DiscountResponse.new(discount: discount)
      end
    end
  end
end

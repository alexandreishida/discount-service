require File.expand_path("../../src/load_path.rb", __FILE__)

require "grpc"
require "grpc-opentracing"
require "jaeger/client"
require "opentracing"

require "grpc_services"
require "mongo_holder"

OpenTracing.global_tracer = Jaeger::Client.build(
                              host: Config::JEAGER_HOST,
                              port: Config::JEAGER_PORT,
                              service_name: Config::TRACER_SERVICE_NAME
                            )

def main
  mongo = MongoHolder::Instance
  mongo[:users].delete_many
  mongo[:users].insert_one({
    _id: "user-1",
    first_name: "User",
    last_name: "1",
    date_of_birth: Date.new(1990,1,1)
  })
  mongo[:users].insert_one({
    _id: "user-2",
    first_name: "User",
    last_name: "2",
    date_of_birth: Date.new(1990,11,25)
  })
  mongo[:users].insert_one({
    _id: "user-3",
    first_name: "User",
    last_name: "3",
    date_of_birth: Date.today
  })

  tracing_interceptor = GRPC::OpenTracing::ServerInterceptor.new(tracer: OpenTracing.global_tracer, decorators: [])

  server = GRPC::RpcServer.new
  port = server.add_http2_port("0.0.0.0:50051", :this_port_is_insecure)
  server.handle(tracing_interceptor.intercept(GrpcServices::DiscountService))

  puts " * Discount Service started"
  puts " * Running on 0.0.0.0:#{port} (grpc)"

  server.run_till_terminated_or_interrupted([1, "int", "SIGQUIT"])
end

main

# Discount Service

## The challenge

This service receives a product id and user id and returns a discount.

Example product:

```
{
     id: string
     price_in_cents: int
     title: string
     description: string
     discount: {
         pct: float
         value_in_cents: int
     }
}
```

User example:
```
{
     id: string
     first_name: string
     last_name: string
     date_of_birth: Date
}
```

The application discount rules are:
- If it is the user's birthday, the product will receive a 5% discount.
- If it is black friday (in this example it can be fixed on 25/11) the product will have a 10% discount.
- The discount can not exceed 10%.

Restrictions:
- The services of this test should be written using different languages.
- The services of this test must communicate via `gRPC` (https://grpc.io/).
- Use docker to provision services.
- For ease of use, services can use a shared database.

## The result

### Requirements
- Ruby 2.6
- Bundler
- Mongo
```bash
$ docker run --rm -d -p 27017:27017 mongo:4
```
- Jaeger
```bash
$ docker run --rm -p 6831:6831/udp -p 6832:6832/udp -p 16686:16686 jaegertracing/all-in-one:1.12 --log-level=debug
```

### Installation
```bash
$ bundle install
$ ./fix-broken-gems.sh
```

### Test
```bash
$ rspec spec
...........

Finished in 0.01651 seconds (files took 0.3735 seconds to load)
11 examples, 0 failures
```

### Run
```bash
$ ruby src/main.rb
```

### CI
- https://gitlab.com/alexandreishida/discount-service/pipelines

### Docker run
```bash
$ docker login registry.gitlab.com
$ docker run --rm -it -p 50051:50051 registry.gitlab.com/alexandreishida/discount-service:0.2.1
```

Some options can be set with `ENV` variables.

| ENV          | Default    |
| -------------| ---------- |
| MONGO_HOST   | 172.17.0.1 |
| MONGO_PORT   | 27017      |
| MONGO_DBNAME | store-dev  |
| JAEGER_HOST  | 172.17.0.1 |
| JAEGER_PORT  | 6831       |

### Instrumentation (Opentracing + Jaeger)
Access the Jaeger UI (http://localhost:16686).

![alt text](https://gitlab.com/alexandreishida/discount-service/raw/master/screenshot-opentracing-jaeger.png "Opentracing + Jaeger")

### To Do
- Improve tracing (tags)
- Improve logging
- Secure GRPC

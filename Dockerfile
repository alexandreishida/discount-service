FROM ruby:2.6

ADD . /app
WORKDIR /app

RUN gem install bundler
RUN bundle install
RUN ./fix-broken-gems.sh

CMD ["ruby", "src/main.rb"]

EXPOSE 50051

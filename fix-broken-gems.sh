mkdir -p tmp
cd tmp

rm -rf ruby-method-tracer
git clone https://github.com/cv/ruby-method-tracer.git
cd ruby-method-tracer
git checkout patch-1
echo "ruby-2.6.0" > .ruby-version
gem build method-tracer.gemspec
gem install method-tracer-1.1.2.gem

cd ..

rm -rf ruby-test-tracer
git clone https://github.com/cv/ruby-test-tracer.git
cd ruby-test-tracer
git checkout patch-2
echo "ruby-2.6.0" > .ruby-version
gem build test-tracer.gemspec
gem install test-tracer-1.2.2.gem

cd ..

rm -rf ruby-grpc-opentracing
git clone https://github.com/cv/ruby-grpc-opentracing.git
cd ruby-grpc-opentracing
git checkout patch-1
echo "ruby-2.6.0" > .ruby-version
gem build grpc-opentracing.gemspec
gem install grpc-opentracing-1.0.0.gem

cd ../..

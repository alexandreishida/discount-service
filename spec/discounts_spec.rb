require "date"

require "discounts"
require "user"

RSpec.describe Discounts do
  let(:user) { User.new(date_of_birth: Date.new(1989,7,19)) }

  describe Discounts::BlackFridayDiscount do
    describe "#get" do
      context "on a day different of the black friday" do
        let(:first_day_of_the_year) { Date.new(2019,1,1) }

        before do
          allow(Date).to receive(:today).and_return(first_day_of_the_year)
        end

        it "should return 0" do
          discount = Discounts::BlackFridayDiscount.new.get(user)
          expect(discount).to eq(0.0)
        end
      end

      context "on black friday" do
        let(:black_friday) { Date.new(2019,11,25) }

        before do
          allow(Date).to receive(:today).and_return(black_friday)
        end

        it "should return 10" do
          discount = Discounts::BlackFridayDiscount.new.get(user)
          expect(discount).to eq(10.0)
        end
      end
    end
  end

  describe Discounts::BirthdayDiscount do
    describe "#get" do
      context "on a day different of the birthday" do
        let(:first_day_of_the_year) { Date.new(2019,1,1) }

        before do
          allow(Date).to receive(:today).and_return(first_day_of_the_year)
        end

        it "should return 0" do
          discount = Discounts::BirthdayDiscount.new.get(user)
          expect(discount).to eq(0.0)
        end
      end

      context "on birthday" do
        before do
          user.date_of_birth = Date.today
        end

        it "should return 5" do
          discount = Discounts::BirthdayDiscount.new.get(user)
          expect(discount).to eq(5.0)
        end
      end
    end
  end
end

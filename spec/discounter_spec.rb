require "date"

require "discounter"
require "discounts"
require "user"

RSpec.describe Discounter do
  let(:black_friday) { Date.new(2019,11,25) }
  let(:user) { User.new(date_of_birth: Date.new(1989,7,19)) }

  describe "#DISCOUNTS" do
    it "should have these discounts" do
      expect(Discounter::DISCOUNTS.map(&:class)).to eq [
        Discounts::BlackFridayDiscount,
        Discounts::BirthdayDiscount
      ]
    end

    it "should be ordered by value desc" do
      values = Discounter::DISCOUNTS.map(&:value)
      expect(values).to eq(values.sort.reverse)
    end
  end

  describe "#MAX" do
    it "should be 10" do
      expect(Discounter::MAX).to eq(10.0)
    end
  end

  describe "#calculate" do
    context "on black friday and birthday" do
      before do
        user.date_of_birth = black_friday
        allow(Date).to receive(:today).and_return(black_friday)
      end

      it "limits the max discount" do
        discount = Discounter.calculate(user)
        expect(discount).to eq(Discounter::MAX)
      end

      it "prevents unnecessary processing" do
        expect(Discounter::DISCOUNTS[1]).not_to receive(:get)

        Discounter.calculate(user)
      end
    end
  end
end

require "date"
require "pry"

RSpec.describe Discounts do
  describe "#find" do
    let(:mongo) { MongoHolder::Instance }

    before do
      mongo[:users].delete_many

      2.times do |i|
        mongo[:users].insert_one({
          _id: "user-#{i}",
          first_name: "User",
          last_name: i.to_s,
          date_of_birth: Date.new(1990, 1, 1)
        })
      end
    end

    context "with valid id" do
      it "returns one" do
        user = User.find("user-0")

        expect(user.id).to eq("user-0")
        expect(user.first_name).to eq("User")
        expect(user.last_name).to eq("0")
        expect(user.date_of_birth).to eq(Date.new(1990, 1, 1))
      end
    end

    context "with invalid id" do
      it "returns nil" do
        user = User.find("invalid-id")
        
        expect(user).to be_nil
      end
    end
  end
end
